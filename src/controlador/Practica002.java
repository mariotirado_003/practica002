package controlador;

import modelo.Cotizaciones;
import vista.dlgCotizaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author luism
 */
public class Practica002 implements ActionListener {
    private Cotizaciones cot;
    private dlgCotizaciones vista;

    public Practica002(Cotizaciones cot, dlgCotizaciones vista){
        this.cot=cot;
        this.vista=vista;
        
        //Hacer que el controlador escuche los botones
        vista.btnnuecot.addActionListener(this);
        vista.btnlimpiar.addActionListener(this);
        vista.btncancelar.addActionListener(this);
        vista.btncerrar.addActionListener(this);
        vista.btnguardar.addActionListener(this);
        vista.btnmostrarcot.addActionListener(this);
    }
    
    private void iniciarVista(){
        vista.setTitle("***_Cotizaciones_***");
        vista.setSize(500,500);
        vista.setVisible(true);   
        
    }
    
    public void limpiar(){
        vista.txtnumcot.setText("");
        vista.txtdescrip.setText("");
        vista.txtprecio.setText("");
        vista.comboboxmeses.setSelectedItem("Select");
        //vista.txtppi.setText("");
        
    }
    private boolean isVacio(){
        if(this.vista.txtnumcot.getText().equals("") || this.vista.txtdescrip.getText().equals("") || this.vista.txtprecio.getText().equals("")){
            return true;
        }
        else{
            return false;
        }
    }
     
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
        
        if(e.getSource()==vista.btnlimpiar){
            limpiar();
        }
        
        if(e.getSource()==vista.btnnuecot){
            vista.txtnumcot.setEnabled(true);
            vista.txtdescrip.setEnabled(true);
            vista.txtprecio.setEnabled(true);
            //vista.btnmostrarcot.setEnabled(true);
            vista.txtppi.setEnabled(true);
            vista.comboboxmeses.setEnabled(true);
        }
        
        if(e.getSource()==vista.btnguardar){
            if(isVacio()==true){
                JOptionPane.showMessageDialog(vista, "ERROR AL CAPTURAR LOS DATOS, NO DEBE HABER CAMPOS VACIOS");
            }
            else{
   
            try{
                cot.Setnumcot(String.valueOf(vista.txtnumcot.getText()));
                cot.Setdescrip(String.valueOf(vista.txtdescrip.getText()));         //REVISARRRRRR
                cot.Setprecio(Float.parseFloat(vista.txtprecio.getText()));
                cot.Setplazo(Integer.parseInt(vista.comboboxmeses.getSelectedItem().toString()));
                cot.setppi(Integer.parseInt(vista.txtppi.getText()));
                JOptionPane.showMessageDialog(vista, "Se agrego con exito!!");
                limpiar();
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgio el siguiente error:" +ex.getMessage());
                }
                 catch(Exception ex2){
                     JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
                     
                 }
            }
        }
        
        if(e.getSource()==vista.btnmostrarcot){
                this.vista.txtnumcot.setText(this.cot.Getnumcot());
                this.vista.txtdescrip.setText(this.cot.Getdescrip());
                this.vista.txtprecio.setText(String.valueOf(this.cot.Getprecio()));
                //this.vista.txtppi.setText(String.valueOf(this.cot.Getppi()));
                //this.vista.txtprecio.setText(String.valueOf(this.cot.Getprecio()));
                this.vista.txtCpain.setText(Float.toString(this.cot.pagoinicial()));
                this.vista.txtCtofi.setText(Float.toString(this.cot.totalfinal()));
                this.vista.txtCpame.setText(Float.toString(this.cot.pagomensual()));
                
        }
        
        if(e.getSource()==vista.btncerrar){
            int option=JOptionPane.showConfirmDialog(vista, "Esta seguro que desea cerrar esta ventana?","Decide",JOptionPane.YES_NO_OPTION);
            
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
            
        }
         if(e.getSource()==vista.btncancelar){
             limpiar();
             vista.txtnumcot.enable(false);
             vista.txtdescrip.enable(false);
             vista.txtprecio.enable(false);
             vista.btnmostrarcot.enable(false);
             vista.btnlimpiar.enable(false);
             vista.btncancelar.enable(false);
         }
    }

    
    public static void main(String[] args) {
        
        Cotizaciones cotiza=new Cotizaciones();
        dlgCotizaciones dlgvista=new dlgCotizaciones();
        
        Practica002 contra=new Practica002(cotiza,dlgvista);
        contra.iniciarVista();
    }
}
