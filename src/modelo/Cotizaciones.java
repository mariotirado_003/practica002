package modelo;

/**
 *
 * @author luism
 */
public class Cotizaciones{
    private int ppi=25,plazo=0;
    private float precio=0;
    private String descrip="",numcot="";
    
    public Cotizaciones(){
        this.ppi=ppi;
        this.plazo=plazo;
        this.descrip=descrip;
        this.precio=precio;
        this.numcot=numcot;
    }
    
    public Cotizaciones(int ppi,int plazo,float precio,String descrip,String numcot){
        this.numcot=numcot;
        
    }
    public Cotizaciones(Cotizaciones aux){
        this.ppi = aux.ppi;
        this.plazo = aux.plazo;
        this.descrip = aux.descrip;
        this.precio = aux.precio;
        this.numcot = aux.numcot;
    }
    
    public void Setnumcot(String numcot){
        this.numcot=numcot;
    }
    public void Setdescrip(String descrip){
        this.descrip=descrip;
    }
    public void Setprecio(float precio){
        this.precio=precio;
    }
    public void Setplazo(int plazo){
        this.plazo=plazo;
    }
    public void setppi(int ppi){
        this.ppi=ppi;
    }
    
    public int getppi(){
        return ppi;
    }
    public String Getnumcot(){
        return numcot;
    }
    public String Getdescrip(){
        return descrip;
    }
    public float Getprecio(){
        return precio;
    }
    

    public float pagoinicial(){
        return precio * ppi/100;
    }
    
    public float totalfinal(){
        return precio - pagoinicial() ;
    }
    
    public float pagomensual(){
        return totalfinal() / plazo;
    }
}
